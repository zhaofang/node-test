let fs = require('fs')
let zlib = require('zlib');
//压缩input.txt文件为input.txt.gz
/*fs.createReadStream('../document/input.txt')
.pipe(zlib.createGzip())
.pipe(fs.createWriteStream('../document/input.txt.gz'))*/
//解压input.txt.gz文件为input_new.txt
fs.createReadStream('./document/input.txt.gz')
    .pipe(zlib.createGunzip())
    .pipe(fs.createWriteStream('./document/input_new.txt'))
console.log('==========over=========')